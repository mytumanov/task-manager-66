package ru.mtumanov.tm.endpoint;

import java.util.ArrayList;
import java.util.Collection;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import ru.mtumanov.tm.api.client.ITaskEndpointClient;
import ru.mtumanov.tm.dto.model.TaskDTO;
import ru.mtumanov.tm.marker.IntegrationCategory;

@Category(IntegrationCategory.class)
public class TaskEndpointTest {
    @NotNull
    private final static String BASE_URL = "http://localhost:8080/api/task";

    @NotNull
    private ITaskEndpointClient taskEndpointClient = ITaskEndpointClient.taskClient(BASE_URL);

    @NotNull
    private Collection<TaskDTO> tasks = new ArrayList<>();

    @Before
    public void init() {
        @NotNull TaskDTO task1 = new TaskDTO("Task 1", "Task 1 desc");
        @NotNull TaskDTO task2 = new TaskDTO("Task 2", "Task 2 desc");
        @NotNull TaskDTO task3 = new TaskDTO("Task 3", "Task 3 desc");
        tasks.add(task1);
        tasks.add(task2);
        tasks.add(task3);
        taskEndpointClient.create(task1);
        taskEndpointClient.create(task2);
        taskEndpointClient.create(task3);
    }

    @After
    public void clear() {
        for (TaskDTO task : taskEndpointClient.findAll()) {
            taskEndpointClient.delete(task);
        }
    }

    @Test
    public void findById() {
        for (TaskDTO task : tasks) {
            Assert.assertEquals(task, taskEndpointClient.findById(task.getId()));
        }
    }

    @Test
    public void findAll() {
        Assert.assertEquals(tasks, taskEndpointClient.findAll());
    }

    @Test
    public void deleteById() {
        for (TaskDTO task : tasks) {
            taskEndpointClient.delete(task.getId());
            Assert.assertNull(taskEndpointClient.findById(task.getId()));
        }
    }

    @Test
    public void deleteByEntity() {
        for (TaskDTO task : tasks) {
            taskEndpointClient.delete(task);
            Assert.assertNull(taskEndpointClient.findById(task.getId()));
        }
    }

    @Test
    public void create() {
        @NotNull TaskDTO taskToCreate = new TaskDTO("Task test", "Task test desc");
        taskEndpointClient.create(taskToCreate);
        Assert.assertEquals(tasks, taskEndpointClient.findAll());
    }
}
