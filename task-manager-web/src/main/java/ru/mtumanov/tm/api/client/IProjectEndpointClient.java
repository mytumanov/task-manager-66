package ru.mtumanov.tm.api.client;

import java.util.Collection;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import feign.Feign;
import ru.mtumanov.tm.dto.model.ProjectDTO;

public interface IProjectEndpointClient {

    static IProjectEndpointClient projectClient(@NotNull final String baseUrl) {
        @NotNull final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        @NotNull final HttpMessageConverters converters = new HttpMessageConverters(converter);
        @NotNull final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(IProjectEndpointClient.class, baseUrl);
    }

    @GetMapping(produces = "application/json")
    Collection<ProjectDTO> findAll();

    @GetMapping(value = "/{id}", produces = "application/json")
    ProjectDTO findById(@PathVariable("id") String id);

    @PostMapping(produces = "application/json")
    ProjectDTO create(@RequestBody ProjectDTO project);

    @DeleteMapping(produces = "application/json")
    void delete(@RequestBody ProjectDTO project);

    @DeleteMapping(value = "/{id}", produces = "application/json")
    void delete(@PathVariable("id") String id);

    @PutMapping(produces = "application/json")
    ProjectDTO update(@RequestBody ProjectDTO project);
    
}
