package ru.mtumanov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.mtumanov.tm.model.User;

@Repository
public interface IUserRepository extends IRepository<User> {

    @NotNull
    User findByLogin(@NotNull String login);

    @NotNull
    User findByEmail(@NotNull String email);

    boolean existsByLogin(@NotNull String login);

    boolean existsByEmail(@NotNull String email);

}
